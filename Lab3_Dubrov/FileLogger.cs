﻿using lab_3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Dubrov
{
    public class FileLogger
    {
        private readonly Logger _logger;
        private readonly FileWriter _fileWriter;

        public FileLogger(Logger logger, FileWriter fileWriter)
        {
            _logger = logger;
            _fileWriter = fileWriter;
        }

        public void Log(string message)
        {
            _logger.Log(message);
            _fileWriter.WriteLine(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
            _fileWriter.WriteLine(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
            _fileWriter.WriteLine(message);
        }
    }
}
