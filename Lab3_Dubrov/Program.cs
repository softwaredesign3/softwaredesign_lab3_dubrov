﻿using lab_3;
using Lab3_Dubrov;
using System;

public class Program
{
    public static void Main(string[] args)
    {
        Logger logger = new Logger();
        FileWriter fileWriter = new FileWriter("log.txt");

        FileLogger fileLogger = new FileLogger(logger, fileWriter);

        fileLogger.Log("Today is a wonderful day.");
        fileLogger.Warn("I still have a lot of laboratory work to do .");
        fileLogger.Error("There is very little time left.");

        Console.WriteLine("Messages have been logged to the console and written to log.txt file.");
    }
}
