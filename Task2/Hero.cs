﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Nation { get; set; }

        public Hero(string name, int age, string nation)
        {
            Name = name;
            Age = age;
            Nation = nation;
        }

        public abstract void Display();
    }
}
