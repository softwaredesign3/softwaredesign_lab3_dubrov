﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    public abstract class InventoryDecorator : Hero
    {
        protected Hero Hero;
        public InventoryDecorator(Hero hero) : base(hero.Name, hero.Age, hero.Nation)
        {
            Hero = hero;
        }
    }
}
