﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Xml.Linq;

namespace Task2
{
    public class Paladin : Hero
    {
        public Paladin(string name, int age, string nation) : base(name, age, nation) { }
        public override void Display()
        {
            Console.WriteLine($"Paladin: {Name}, Age: {Age}, Nation: {Nation}");
        }
    }
}
