﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<Hero> heroes = new List<Hero>();
            while (true)
            {
                Console.WriteLine("1. Create Hero");
                Console.WriteLine("2. Display Heroes");
                Console.WriteLine("3. Exit");
                Console.WriteLine();
                Console.Write("Choose an option: ");
                
                string option = Console.ReadLine();

                if (option == "1")
                {
                    Console.WriteLine();
                    Console.Write("Enter Hero Name: ");
                    string name = Console.ReadLine();
                    Console.Write("Enter Hero Age: ");
                    int age = int.Parse(Console.ReadLine());
                    Console.Write("Enter Hero Nation: ");
                    string nation = Console.ReadLine();
                    Console.WriteLine("Choose Hero Class (1-Warrior, 2-Mage, 3-Paladin): ");
                    string heroClass = Console.ReadLine();
                    Hero hero = heroClass switch
                    {
                        "1" => new Warrior(name, age, nation),
                        "2" => new Mage(name, age, nation),
                        "3" => new Paladin(name, age, nation),
                        _ => throw new InvalidOperationException("Invalid Hero Class"),
                    };
                    Console.WriteLine("Choose Inventory Items (armor, weapon, artifact): ");
                    string[] inventoryItems = Console.ReadLine().Split(',');

                    foreach (string item in inventoryItems)
                    {
                        switch (item.Trim().ToLower())
                        {
                            case "armor":
                                hero = new Armor(hero);
                                break;
                            case "weapon":
                                hero = new Weapon(hero);
                                break;
                            case "artifact":
                                hero = new Artifact(hero);
                                break;
                        }
                    }

                    heroes.Add(hero);
                }
                else if (option == "2")
                {
                    foreach (var hero in heroes)
                    {
                        hero.Display();
                        Console.WriteLine();
                    }
                }
                else if (option == "3")
                {
                    break;
                }
            }
        }
    }
}
