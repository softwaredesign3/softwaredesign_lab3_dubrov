﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    public class Warrior : Hero
    {
        public Warrior(string name, int age, string nation) : base(name, age, nation) { }
        public override void Display()
        {
            Console.WriteLine($"Warrior: {Name}, Age: {Age}, Nation: {Nation}");
        }
    }
}
