﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    public class RasterRenderer : Renderer
    {
        private Panel graphicsPanel;

        public RasterRenderer(Panel graphicsPanel)
        {
            this.graphicsPanel = graphicsPanel;
        }

        public void Render(string shape)
        {
            using (Graphics graphics = graphicsPanel.CreateGraphics())
            {
                graphics.Clear(Color.White);
                Pen pen = new Pen(Color.Black);
                Brush brush = new SolidBrush(Color.Black);

                switch (shape)
                {
                    case "Circle":
                        DrawPixelatedCircle(graphics, pen, brush);
                        break;

                    case "Square":
                        DrawPixelatedSquare(graphics, pen, brush);
                        break;

                    case "Triangle":
                        DrawPixelatedTriangle(graphics, pen, brush);
                        break;
                }
            }
            Console.WriteLine($"Drawing {shape} as pixels");
        }
        private void DrawPixelatedCircle(Graphics graphics, Pen pen, Brush brush)
        {
            int centerX = graphicsPanel.Width / 2;
            int centerY = graphicsPanel.Height / 2;
            int radius = 50; 
            for (int y = -radius; y <= radius; y++)
            {
                for (int x = -radius; x <= radius; x++)
                {
                    if (x * x + y * y <= radius * radius)
                    {
                        graphics.FillRectangle(brush, centerX + x, centerY + y, 1, 1);
                    }
                }
            }
        }

        private void DrawPixelatedSquare(Graphics graphics, Pen pen, Brush brush)
        {
            int centerX = graphicsPanel.Width / 2;
            int centerY = graphicsPanel.Height / 2;
            int size = 100; 
            for (int y = centerY - size / 2; y <= centerY + size / 2; y++)
            {
                for (int x = centerX - size / 2; x <= centerX + size / 2; x++)
                {
                    graphics.FillRectangle(brush, x, y, 1, 1);
                }
            }
        }

        private void DrawPixelatedTriangle(Graphics graphics, Pen pen, Brush brush)
        {
            int centerX = graphicsPanel.Width / 2;
            int centerY = graphicsPanel.Height / 2;
            int size = 100;

            Point top = new Point(centerX, centerY - size / 2);
            Point left = new Point(centerX - size / 2, centerY + size / 2);
            Point right = new Point(centerX + size / 2, centerY + size / 2);

            for (int y = top.Y; y <= left.Y; y++)
            {
                float leftX = Interpolate(top.Y, top.X, left.Y, left.X, y);
                float rightX = Interpolate(top.Y, top.X, right.Y, right.X, y);
                for (int x = (int)leftX; x <= rightX; x++)
                {
                    graphics.FillRectangle(brush, x, y, 1, 1);
                }
            }
        }
        private float Interpolate(int y0, int x0, int y1, int x1, int y)
        {
            if (y0 == y1)
            {
                return x0;
            }
            return x0 + (x1 - x0) * (float)(y - y0) / (y1 - y0);
        }
    }
}