﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    public class VectorRenderer : Renderer
    {
        private Panel panel;

        public VectorRenderer(Panel panel)
        {
            this.panel = panel;
        }

        public void Render(string shape)
        {
            using (Graphics graphics = panel.CreateGraphics())
            {
                graphics.Clear(Color.White);
                Pen pen = new Pen(Color.Black);

                switch (shape)
                {
                    case "Circle":
                        int centerXCircle = panel.Width / 2;
                        int centerYCircle = panel.Height / 2;
                        int sizeCircle = 100;
                        graphics.DrawEllipse(pen, centerXCircle - sizeCircle / 2, centerYCircle - sizeCircle / 2, sizeCircle, sizeCircle);
                        break;

                    case "Square":
                        int centerXSquare = panel.Width / 2;
                        int centerYSquare = panel.Height / 2;
                        int sizeSquare = 100;
                        graphics.DrawRectangle(pen, centerXSquare - sizeSquare / 2, centerYSquare - sizeSquare / 2, sizeSquare, sizeSquare);
                        break;

                    case "Triangle":
                        int centerXTriangle = panel.Width / 2;
                        int centerYTriangle = panel.Height / 2;
                        int sizeTriangle = 100;
                        Point[] points = {
                            new Point(centerXTriangle, centerYTriangle - sizeTriangle / 2),
                            new Point(centerXTriangle - sizeTriangle / 2, centerYTriangle + sizeTriangle / 2),
                            new Point(centerXTriangle + sizeTriangle / 2, centerYTriangle + sizeTriangle / 2)
                        };
                        graphics.DrawPolygon(pen, points);
                        break;
                }
            }
            Console.WriteLine($"Drawing {shape} as vectors");
        }
    }
}