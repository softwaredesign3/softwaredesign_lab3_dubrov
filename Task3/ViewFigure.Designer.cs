﻿namespace Task3
{
    partial class ViewFigure
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.graphicsPanel = new System.Windows.Forms.Panel();
            this.shapeComboBox = new System.Windows.Forms.ComboBox();
            this.renderComboBox = new System.Windows.Forms.ComboBox();
            this.drawButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
           
            this.graphicsPanel.BackColor = System.Drawing.Color.White;
            this.graphicsPanel.Location = new System.Drawing.Point(12, 12);
            this.graphicsPanel.Name = "graphicsPanel";
            this.graphicsPanel.Size = new System.Drawing.Size(260, 200);
            this.graphicsPanel.TabIndex = 0;
            this.graphicsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.graphicsPanel_Paint_1);
          
            this.shapeComboBox.FormattingEnabled = true;
            this.shapeComboBox.Items.AddRange(new object[] {
            "Circle",
            "Square",
            "Triangle"});
            this.shapeComboBox.Location = new System.Drawing.Point(12, 218);
            this.shapeComboBox.Name = "shapeComboBox";
            this.shapeComboBox.Size = new System.Drawing.Size(121, 28);
            this.shapeComboBox.TabIndex = 1;
          
            this.renderComboBox.FormattingEnabled = true;
            this.renderComboBox.Items.AddRange(new object[] {
            "Vector",
            "Raster"});
            this.renderComboBox.Location = new System.Drawing.Point(151, 218);
            this.renderComboBox.Name = "renderComboBox";
            this.renderComboBox.Size = new System.Drawing.Size(121, 28);
            this.renderComboBox.TabIndex = 2;
           
            this.drawButton.Location = new System.Drawing.Point(197, 252);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(75, 23);
            this.drawButton.TabIndex = 3;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
          
            this.ClientSize = new System.Drawing.Size(284, 287);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.renderComboBox);
            this.Controls.Add(this.shapeComboBox);
            this.Controls.Add(this.graphicsPanel);
            this.Name = "ViewFigure";
            this.Text = "Shape Drawer";
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Panel graphicsPanel;
        private System.Windows.Forms.ComboBox shapeComboBox;
        private System.Windows.Forms.ComboBox renderComboBox;
        private System.Windows.Forms.Button drawButton;
    }
}