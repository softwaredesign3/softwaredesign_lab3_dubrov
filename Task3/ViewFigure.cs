﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task3
{
    public partial class ViewFigure : Form
    {
        private string selectedShape;
        private string selectedRenderer;
        private Renderer renderer;
        private Shape shape;
        public ViewFigure()
        {
            InitializeComponent();
            this.graphicsPanel.Size = new System.Drawing.Size(200, 200);
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            selectedShape = shapeComboBox.SelectedItem?.ToString();
            selectedRenderer = renderComboBox.SelectedItem?.ToString();

            if (selectedShape == null || selectedRenderer == null)
            {
                MessageBox.Show("Please select both shape and renderer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            switch (selectedRenderer)
            {
                case "Raster":
                    renderer = new RasterRenderer(graphicsPanel);
                    break;
                case "Vector":
                    renderer = new VectorRenderer(graphicsPanel);
                    break;
                default:
                    MessageBox.Show("Unknown renderer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            switch (selectedShape)
            {
                case "Circle":
                    shape = new Circle(renderer);
                    break;
                case "Square":
                    shape = new Square(renderer);
                    break;
                case "Triangle":
                    shape = new Triangle(renderer);
                    break;
                default:
                    MessageBox.Show("Unknown shape.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            graphicsPanel.Invalidate();
        }

        private void graphicsPanel_Paint_1(object sender, PaintEventArgs e)
        {
            if (shape != null)
            {
                shape.Draw();
            }
        }
    }
}