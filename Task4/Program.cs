using System.IO;

namespace Task4
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            TextReader smartTextReader = new SmartTextReader();
            TextReader smartTextChecker = new SmartTextChecker(smartTextReader);
            TextReader smartTextReaderLocker = new SmartTextReaderLocker(smartTextChecker, @"^.*restricted.*$");

            string filePath = "Text.txt";
            string restrictedFilePath = "Text_restricted.txt";
            char[][] result = smartTextReaderLocker.ReadFile(filePath);
            if (result != null)
            {
                foreach (var line in result)
                {
                    Console.WriteLine(new string(line));
                }
            }
            smartTextReaderLocker.ReadFile(restrictedFilePath);
        }
    }
}