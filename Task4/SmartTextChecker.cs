﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task4
{
    public class SmartTextChecker : TextReader
    {
        private TextReader _smartTextReader;

        public SmartTextChecker(TextReader smartTextReader)
        {
            _smartTextReader = smartTextReader;
        }

        public char[][] ReadFile(string filePath)
        {
            Console.WriteLine($"Opening file: {filePath}");
            char[][] result = _smartTextReader.ReadFile(filePath);
            Console.WriteLine($"File read successfully: {filePath}");
            int lineCount = result.Length;
            int charCount = 0;
            foreach (var line in result)
            {
                charCount += line.Length;
            }
            Console.WriteLine($"Total lines: {lineCount}");
            Console.WriteLine($"Total characters: {charCount}");
            return result;
        }
    }
}