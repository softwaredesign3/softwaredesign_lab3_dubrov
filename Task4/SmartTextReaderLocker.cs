﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task4
{
    public class SmartTextReaderLocker : TextReader
    {
        private TextReader _smartTextReader;
        private Regex _accessPattern;

        public SmartTextReaderLocker(TextReader smartTextReader, string pattern)
        {
            _smartTextReader = smartTextReader;
            _accessPattern = new Regex(pattern);
        }

        public char[][] ReadFile(string filePath)
        {
            if (_accessPattern.IsMatch(filePath))
            {
                Console.WriteLine("Access denied!");
                return null;
            }
            return _smartTextReader.ReadFile(filePath);
        }
    }
}
