﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    public class LightElementNode : LightNode
    {
        public string TagName { get; private set; }
        public string DisplayType { get; private set; }
        public string ClosingType { get; private set; }
        public List<string> CssClasses { get; private set; }
        public List<LightNode> Children { get; private set; }

        public LightElementNode(string tagName, string displayType, string closingType)
        {
            TagName = tagName;
            DisplayType = displayType;
            ClosingType = closingType;
            CssClasses = new List<string>();
            Children = new List<LightNode>();
        }

        public void AddClass(string className)
        {
            CssClasses.Add(className);
        }

        public void AddChild(LightNode child)
        {
            Children.Add(child);
        }

        public override string OuterHTML
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append($"<{TagName}");

                if (CssClasses.Count > 0)
                {
                    sb.Append($" class=\"{string.Join(" ", CssClasses)}\"");
                }

                if (ClosingType == "self-closing")
                {
                    sb.Append(" />");
                }
                else
                {
                    sb.Append(">");
                    sb.Append(InnerHTML);
                    sb.Append($"</{TagName}>");
                }

                return sb.ToString();
            }
        }

        public override string InnerHTML
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                foreach (var child in Children)
                {
                    sb.Append(child.OuterHTML);
                }

                return sb.ToString();
            }
        }
    }
}
