namespace Task5
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            LightElementNode div = new LightElementNode("div", "block", "normal");
            div.AddClass("message");

            LightElementNode p = new LightElementNode("h2", "block", "normal");
            p.AddClass("message");

            LightTextNode textNode = new LightTextNode("My name is Misha");

            p.AddChild(textNode);
            div.AddChild(p);
            Console.WriteLine(div.OuterHTML);
        }
    }
}