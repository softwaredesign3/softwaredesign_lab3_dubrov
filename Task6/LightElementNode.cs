﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    public class LightElementNode
    {
        public string TagName { get; }
        public string Display { get; }
        public string FontWeight { get; }
        public List<LightElementNode> Children { get; }
        public List<LightTextNode> TextNodes { get; }

        public LightElementNode(string tagName, string display, string fontWeight)
        {
            TagName = tagName;
            Display = display;
            FontWeight = fontWeight;
            Children = new List<LightElementNode>();
            TextNodes = new List<LightTextNode>();
        }

        public void AddChild(LightElementNode child)
        {
            Children.Add(child);
        }

        public void AddChild(LightTextNode textNode)
        {
            TextNodes.Add(textNode);
        }

        public string GetInnerText(bool includeTags = false)
        {
            StringBuilder sb = new StringBuilder();

            if (includeTags)
            {
                sb.Append($"<{TagName}>");
            }

            foreach (var textNode in TextNodes)
            {
                sb.AppendLine(textNode.Text);
            }

            foreach (var child in Children)
            {
                sb.AppendLine(child.GetInnerText(includeTags));
            }

            if (includeTags)
            {
                sb.Append($"</{TagName}>");
            }

            return sb.ToString().TrimEnd();
        }
    }
}