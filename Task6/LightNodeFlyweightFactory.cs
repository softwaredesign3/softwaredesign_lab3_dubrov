﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    public class LightNodeFlyweightFactory
    {
        public LightElementNode CreateElementNode(string tagName, string display, string fontWeight)
        {
            return new LightElementNode(tagName, display, fontWeight);
        }
    }
}