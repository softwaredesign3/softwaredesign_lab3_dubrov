﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    public class LightTextNode  
    {
        public string Text { get; }

        public LightTextNode(string text)
        {
            Text = text;
        }
    }
}