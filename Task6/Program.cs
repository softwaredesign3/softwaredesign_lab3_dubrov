﻿    using System;
    using System.IO;
    using System.Net.Http;
    using System.Text;
    using System.Collections.Generic;
    using System.Text.Json;
    namespace Task6
    {
        class Program
        {
        static async Task Main(string[] args)
        {
            string bookUrl = "https://www.gutenberg.org/cache/epub/1513/pg1513.txt";
            string bookText;

            using (HttpClient client = new HttpClient())
            {
                bookText = await client.GetStringAsync(bookUrl);
            }

            LightNodeFlyweightFactory factory = new LightNodeFlyweightFactory();

            using (StringReader reader = new StringReader(bookText))
            {
                string line;
                bool isFirstLine = true;
                LightElementNode currentParagraph = null;

                int textNodeCount = 0;
                int elementNodeCount = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        if (currentParagraph != null)
                        {
                            Console.WriteLine(currentParagraph.GetInnerText(true));
                            currentParagraph = null;
                        }
                        Console.WriteLine();
                        continue;
                    }

                    LightElementNode element = null;

                    if (isFirstLine)
                    {
                        element = factory.CreateElementNode("h1", "block", "normal");
                        isFirstLine = false;
                    }
                    else if (line.Length < 20)
                    {
                        element = factory.CreateElementNode("h2", "block", "normal");
                    }
                    else if (line.StartsWith(" "))
                    {
                        element = factory.CreateElementNode("blockquote", "block", "normal");
                    }
                    else
                    {
                        if (currentParagraph == null)
                        {
                            currentParagraph = factory.CreateElementNode("p", "block", "normal");
                        }
                        element = currentParagraph;
                    }

                    LightTextNode textNode = new LightTextNode(line);
                    element.AddChild(textNode);

                    if (element != currentParagraph)
                    {
                        Console.WriteLine(element.GetInnerText(true));
                    }

                    textNodeCount++;
                    elementNodeCount++;
                }

                if (currentParagraph != null)
                {
                    Console.WriteLine(currentParagraph.GetInnerText(true));
                    elementNodeCount++;
                }

                int textNodeMemory = textNodeCount * 128;
                int elementNodeMemory = elementNodeCount * 256; 
                int totalMemory = textNodeMemory + elementNodeMemory;

                Console.WriteLine($"Кiлькiсть вузлiв тексту: {textNodeCount}");
                Console.WriteLine($"Кiлькiсть вузлiв елементiв: {elementNodeCount}");
                Console.WriteLine($"Загальний обсяг пам'ятi: {totalMemory} байт.");
            }
        }
    }
}